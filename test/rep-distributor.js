/*global web3, contract, before, it, context*/
/*eslint no-undef: "error"*/

const { expect } = require('chai');
const { time, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');
const helpers = require('./helpers');
const { toWei } = web3.utils;

const deploy = async (accounts) => {
    // initialize test setup
    const setup = await helpers.setup.initialize(accounts[0]);
    // deploy ERC20s
    setup.tokens = await helpers.setup.tokens(setup);
    // deploy DAOStack meta-contracts
    setup.DAOStack = await helpers.setup.DAOStack(setup);
    // deploy organization
    setup.organization = await helpers.setup.organization(setup);
    // deploy distributor
    setup.repDistributor = await helpers.setup.repDistributor(setup);

    return setup;
};

contract('RepDistributor', (accounts) => {
    let setup;

    before('!! deploy setup', async () => {
        setup = await deploy(accounts);
    });

    context('» distributor reputation change', () => {

        it('it add/remove token from address which is not an owner', async () => {
            await expectRevert(setup.repDistributor.add(accounts[0],{from:accounts[1]}), 'Ownable: caller is not the owner');
            await expectRevert(setup.repDistributor.remove(accounts[0],{from:accounts[1]}), 'Ownable: caller is not the owner');
            await expectRevert(setup.repDistributor.set_avatar(accounts[1],{from:accounts[1]}), 'Ownable: caller is not the owner');
        });

        it('it add and remove tokens from/to some address', async () => {
            const balanceBefore = await setup.organization.reputation.balanceOf(accounts[0]);
            setup.repDistributor.add(accounts[0]);
            const balanceAfterAdd = await setup.organization.reputation.balanceOf(accounts[0]);
            setup.repDistributor.remove(accounts[0]);
            const balanceAfterBurn = await setup.organization.reputation.balanceOf(accounts[0]);
            expect(Number(balanceBefore)+Number(web3.utils.toWei('1','ether'))).to.be.equal(Number(balanceAfterAdd));
            expect(Number(balanceBefore)).to.be.equal(Number(balanceAfterBurn));
        });

        it('it remove tokens from empty address', async () => {
            const balanceBefore = await setup.organization.reputation.balanceOf(accounts[1]);
            setup.repDistributor.remove(accounts[1]);
            const balanceAfterBurn = await setup.organization.reputation.balanceOf(accounts[1]);
            expect(Number(balanceBefore)).to.be.equal(Number(balanceAfterBurn));
        });

        it('it updates avatar address', async () => {
            setup.repDistributor.set_avatar(accounts[1]);
            expect(await  setup.repDistributor.avatar()).to.be.equal(accounts[1]);
        });
    });
});

