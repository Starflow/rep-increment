const ERC20 = artifacts.require('ERC20Mock');
const ControllerCreator = artifacts.require('./ControllerCreator.sol');
const DaoCreator = artifacts.require('./DaoCreator.sol');
const DAOTracker = artifacts.require('./DAOTracker.sol');
const Avatar = artifacts.require('./Avatar.sol');
const Controller = artifacts.require('./Controller.sol');
const DAOToken = artifacts.require('./DAOToken.sol');
const Reputation = artifacts.require('./Reputation.sol');
const PrimeToken = artifacts.require('PrimeToken');
const RepDistributor = artifacts.require('RepDistributor');


const { constants } = require('@openzeppelin/test-helpers');
// Incentives imports

const MAX = web3.utils.toTwosComplement(-1);

const { toWei } = web3.utils;
const { fromWei } = web3.utils;

const PDAO_TOKENS = toWei('1000');
const PRIME_CAP = toWei('90000000');
const PRIME_SUPPLY = toWei('21000000');
const REPUTATION = '1000';

const deployOrganization = async (daoCreator, daoCreatorOwner, founderToken, founderReputation, cap = 0) => {
    var org = {};
    var tx = await daoCreator.forgeOrg('primeDAO', 'PrimeDAO token', 'PDAO', daoCreatorOwner, founderToken, founderReputation, cap, { gas: constants.ARC_GAS_LIMIT });
    assert.equal(tx.logs.length, 1);
    assert.equal(tx.logs[0].event, 'NewOrg');
    var avatarAddress = tx.logs[0].args._avatar;
    org.avatar = await Avatar.at(avatarAddress);
    var tokenAddress = await org.avatar.nativeToken();
    org.token = await DAOToken.at(tokenAddress);
    var reputationAddress = await org.avatar.nativeReputation();
    org.reputation = await Reputation.at(reputationAddress);
    return org;
};


const initialize = async (root) => {
    const setup = {};
    setup.root = root;
    setup.data = {};
    setup.data.balances = [];
    return setup;
};

const tokens = async (setup) => {
    const erc20s = [await ERC20.new('DAI Stablecoin', 'DAI', 18), await ERC20.new('USDC Stablecoin', 'USDC', 15), await ERC20.new('USDT Stablecoin', 'USDT', 18)];

    const primeToken = await PrimeToken.new(PRIME_SUPPLY, PRIME_CAP, setup.root);

    return { erc20s, primeToken};
};


const repDistributor = async (setup) => {
    const repDistributor = await RepDistributor.new();
    const controller = await Controller.at(await setup.organization.avatar.owner());
    console.log(await repDistributor.avatar());
    console.log(await repDistributor.address);
    // console.log(await controller.schemes(accounts[0]));
    // await controller.registerScheme(repDistributor.address, '0x000000000000000000000000000000', '0x1111', setup.organization.avatar.address);

    const permissions = '0x00000010';
    await setup.DAOStack.daoCreator.setSchemes(
        setup.organization.avatar.address,
        [repDistributor.address],
        [constants.ZERO_BYTES32],
        [permissions],
        'metaData'
    );
    repDistributor.set_avatar(setup.organization.avatar.address);
    console.log(await repDistributor.avatar());
    console.log(await controller.schemes(repDistributor.address));
    return repDistributor;
};

const DAOStack = async () => {
    const controllerCreator = await ControllerCreator.new();
    const daoTracker = await DAOTracker.new();
    const daoCreator = await DaoCreator.new(controllerCreator.address, daoTracker.address);

    return { controllerCreator, daoTracker, daoCreator };
};

const organization = async (setup) => {
    // deploy organization
    const organization = await deployOrganization(setup.DAOStack.daoCreator, [setup.root], [PDAO_TOKENS], [REPUTATION]);

    return organization;
};




module.exports = {
    initialize,
    tokens,
    DAOStack,
    organization,
    repDistributor
};
