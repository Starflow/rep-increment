pragma solidity ^0.5.13;
//pragma experimental ABIEncoderV2;

import "@daostack/arc/contracts/controller/Controller.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";


/**
 * @title A contract for reputation distribution
 */

contract RepDistributor is Ownable{

    event addREP(address _avatar, address _member);
    event remREP(address _avatar, address _member);

    Avatar public avatar;

    function set_avatar(Avatar _avatar) public onlyOwner{
        avatar = _avatar;
    }

    function add(address to) public onlyOwner{
        Controller(avatar.owner()).mintReputation(1 ether, to, address(avatar) );
        emit addREP(address(avatar), to);
    }

    function remove(address from) public onlyOwner{
        Controller(avatar.owner()).burnReputation(1 ether, from, address(avatar));
        emit remREP(address(avatar), from);
    }
}
