const PrimeToken = artifacts.require('PrimeToken');
const RepDistributor = artifacts.require('RepDistributor');


const contracts = require('../contractAddresses.json');
const fs = require("fs");

module.exports = async function (deployer, network) {
    const { toWei } = web3.utils;
    const primeSupply = 100000000000000;
    if (network === 'mainnet') {
        await deployer.deploy(PrimeToken, primeSupply, primeSupply*10, '0x51d75a031293034dc1a95d8E14D06b8Bf6303379');
        await deployer.deploy(RepDistributor);

        contracts.kovan.PrimeToken = PrimeToken.address;
        contracts.mainnet.RepDistributor = RepDistributor.address;

        // overwrite contranctAddresses.json
        fs.writeFile('./contractAddresses.json', JSON.stringify(contracts), (err) => {
            if (err) throw err;
        });

    } else if (network === 'kovan') {
        await deployer.deploy(PrimeToken, primeSupply, primeSupply*10, '0x51d75a031293034dc1a95d8E14D06b8Bf6303379');
        await deployer.deploy(RepDistributor);

        contracts.kovan.PrimeToken = PrimeToken.address;
        contracts.kovan.RepDistributor = RepDistributor.address;

        // overwrite contranctAddresses.json
        fs.writeFile('./contractAddresses.json', JSON.stringify(contracts), (err) => {
            if (err) throw err;
        });
    } else if (network ==='development'){
        await deployer.deploy(PrimeToken, primeSupply, primeSupply*10, '0x51d75a031293034dc1a95d8E14D06b8Bf6303379');
        await deployer.deploy(RepDistributor);
    }
};
